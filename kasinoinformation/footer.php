	</div>
	<footer class="kasino-footer" id="kasino-footer">
        <div class="container no-padding">
            <div class="kasino-copyright"><img src="<?php echo get_theme_logo(); ?>" alt="<?php bloginfo('name'); ?>" /> &copy; <?php echo date('Y') ?></div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>